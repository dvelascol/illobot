import os
import discord

from dotenv import load_dotenv
from discord.ext import commands
from discord.ext.commands import bot
from discord import FFmpegPCMAudio
import asyncio

load_dotenv()

TOKEN = os.getenv('DISCORD_TOKEN')

intents = discord.Intents.all()

bot = commands.Bot(intents=intents, command_prefix='/')

@bot.event
async def on_ready():
    guild = discord.utils.get(bot.guilds)
    #channel = bot.get_channel(channel_id)
    print("GUILD", guild, "CHANNEL")

@bot.command(name='andres')
async def andres_vc(ctx):
    connected = ctx.author.voice
    if connected:
        try:
            voice_channel = await connected.channel.connect()
            audio_file_path = 'andres.ogg'
            audio_source = FFmpegPCMAudio(audio_file_path, executable="ffmpeg")
            voice_channel.play(audio_source)
        
            while voice_channel.is_playing():
                await asyncio.sleep(1)

            await voice_channel.disconnect()

        except Exception as andresException:
            print(f'La cague: {andresException}' )
            await ctx.send("No he alcanzado el illoOps")

bot.run(TOKEN)